"""
Serial emulator, originall from http://www.science.smith.edu/dftwiki/index.php/PySerial_Simulator

Modified for use with radiation monitors
"""

import random
import struct

class VirtSerial:

    def __init__( self, port='COM1', baudrate = 19200, timeout=1,
                  bytesize = 8, parity = 'N', stopbits = 1, xonxoff=0,
                  rtscts = 0):
        self.name     = port
        self.port     = port
        self.timeout  = timeout
        self.parity   = parity
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.stopbits = stopbits
        self.xonxoff  = xonxoff
        self.rtscts   = rtscts
        self._isOpen  = True
        self._receivedData = ""
        self._data = None

    def isOpen( self ):
        return self._isOpen

    def open( self ):
        self._isOpen = True

    def close( self ):
        self._isOpen = False

    def write( self, string ):
        self._data = None
        self.handle_command(string)

    def read( self, n=1 ):
        if self._data is None or len(self._data) == 0:
            # must be a heartbeat?
            retval = random.randint(0, 1000)
            self._data = struct.pack("!H", retval)
        s = self._data[0:n]
        self._data = self._data[n:]
        return s

    def handle_command(self, command):
        encoded_command = command.decode()
        raw_command = encoded_command.strip("<>")

        if raw_command == "GETVER":
            self._data = "_GQ-SIMULATOR_".encode()

        if raw_command == "GETSERIAL":
            rList = [255]*7
            self._data = bytearray(rList)

        if raw_command == "GETCPM":
            retval = random.randint(0, 1000)
            self._data = struct.pack("!H", retval)

    def flush(self):
        pass
