from driver.gmc_driver import GmcDriver
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test the GQ interface.')
    parser.add_argument('serial_port', type=str,
                        help='Name of the serial device port')

    args = parser.parse_args()

    gd = GmcDriver(args.serial_port)
    print(gd.get_version())
    print(gd.get_serial_number())
    print(gd.get_cpm())
    print(gd.get_history(0, 100))
    # print(gd.get_configuration())